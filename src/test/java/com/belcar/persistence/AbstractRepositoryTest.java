package com.belcar.persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class AbstractRepositoryTest {

    // When using MyBatis-Spring, the MyBatis SqlSessions are managed by the default Spring transaction manager
    // So wee need to use this to be able to rollback after each test
    @Autowired
    protected PlatformTransactionManager txManager;

    protected TransactionStatus txStatus;

    @Before
    public void startTransaction() {
         txStatus = txManager.getTransaction(new DefaultTransactionDefinition());
    }
    @After
    public void rollBackAtEndOfeachTest() {
        txManager.rollback(txStatus);
    }
}
