package com.belcar.persistence;

import com.belcar.domain.Child;
import com.belcar.domain.Daughter;
import com.belcar.domain.Person;
import com.belcar.domain.Son;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PersonWithChildrenRepositoryTest extends AbstractRepositoryTest {


    @Autowired
    PersonWithChildrenRepository personWithChildrenRepository;

    @Test
    public void getAllPersonsShouldContainAllChildren() {
        List<Person> result =  personWithChildrenRepository.getAllPersonsWithChildren();
        assertEquals(1, result.size());
        Person p = result.get(0);
        assertEquals(2, p.getChildren().size());
        for (Child c : p.getChildren()) {
            switch (c.getName()) {
                case "Baron" : assertEquals(Son.class, c.getClass());
                break;
                case "Ivanka" : assertEquals(Daughter.class, c.getClass());
                break;
                default: fail();
            }
        }
    }
}
