package com.belcar.persistence;

import com.belcar.domain.Person;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

public class PersonRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    PersonRepository personRepository;

    @Test
    public void getAllPersonsShouldReturnThreeResults() {
        List<Person> result =  personRepository.getAllPersons();
        assertEquals(3, result.size());
    }

    @Test
    public void newPersonIsPersistedCorrectly() {
        Person goofy = new Person("Goofy");
        Long id = personRepository.storePerson(goofy);
        assertNotNull(id);
        assertEquals(4, personRepository.getAllPersons().size());
    }

}
