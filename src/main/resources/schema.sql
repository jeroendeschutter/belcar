
CREATE TABLE IF NOT EXISTS person(
 id int auto_increment primary key,
 name varchar(200) NOT NULL
);

CREATE TABLE IF NOT EXISTS child(
 id int auto_increment primary key,
 name varchar(200) NOT NULL,
 gender varchar(1) NOT NULL,
 parent int NOT NULL,
 foreign key (parent) references person(id)
);

insert into person(name) values('Donald Duck');
insert into person(name) values('Donald Trump');
insert into person(name) values('Mickey Mouse');
insert into child(name, gender, parent) values('Baron', 'M', (select id from person where name = 'Donald Trump'));
insert into child(name, gender, parent) values('Ivanka', 'F', (select id from person where name = 'Donald Trump'));