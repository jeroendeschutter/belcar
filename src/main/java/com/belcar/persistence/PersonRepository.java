package com.belcar.persistence;

import com.belcar.domain.Person;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface PersonRepository {

    @Select("select * from person")
    List<Person> getAllPersons();

    @Insert("insert into person(name) values(#{name})")
    @Options(useGeneratedKeys=true, keyProperty="id")
    Long storePerson(Person person);
}
