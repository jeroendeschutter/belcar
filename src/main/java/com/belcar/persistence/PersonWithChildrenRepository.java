package com.belcar.persistence;

import com.belcar.domain.Person;

import java.util.List;

public interface PersonWithChildrenRepository {

    List<Person> getAllPersonsWithChildren();
}
