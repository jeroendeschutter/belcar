package com.belcar.rest;

import com.belcar.domain.Person;
import com.belcar.persistence.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/person")
public class PersonController {

    @Autowired
    PersonRepository personRepository;

    @RequestMapping(value = "/all")
    public List<Person> allPersons() {
        return personRepository.getAllPersons();
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void savePerson(@RequestBody Person person) {
        personRepository.storePerson(person);
    }


}
